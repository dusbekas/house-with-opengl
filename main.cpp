#include <GLFW/glfw3.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>

#define WIDTH 1280
#define HEIGHT 700

static void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
    if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
        glfwSetWindowShouldClose(window, GL_TRUE); // close program on ESC key
}

void setup_viewport(GLFWwindow* window)
{
    // setting viewports size, projection etc
    //float ratio;
    int width, height;
    glfwGetFramebufferSize(window, &width, &height);
    //ratio = width / (float) height;
    glViewport(0, 0, width, height);

    glClear(GL_COLOR_BUFFER_BIT);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glOrtho(0.f, WIDTH, 0.f, HEIGHT, 1.0f, -1.0f);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
}

void sky(){
  glBegin(GL_POLYGON);
  glColor3ub(128,229,255);
    glVertex2f(0, 700);
  glColor3ub(213,229,255);
     glVertex2f(1280, 700);
  glColor3ub(128,229,255);
     glVertex2f(1280, 380);
     glVertex2f(0, 380);
  glEnd();
}

void land(){
  glBegin(GL_POLYGON);
  glColor3ub(1,166,17);
    glVertex2f(0, 380);
  glColor3ub(1,190,17);
    glVertex2f(1280, 380);
  glColor3ub(1,166,17);
    glVertex2f(1280, 0);
    glVertex2f(0, 0);
  glEnd();
}

void path(){
  glColor3ub(210,210,210);
  glBegin(GL_POLYGON);
  glVertex2f(911.369, 219.669);
  glVertex2f(1280, 124.774);
  glVertex2f(1280, 47.992);
  glVertex2f(911.369, 157.079);
  glEnd();
}

void footPath(){
  glColor3ub(210,210,210);
  glBegin(GL_POLYGON);
  glVertex2f(525.058, 99.475);
  glVertex2f(669.759, 128.064);
  glVertex2f(683.554, 120.238);
  glVertex2f(539.619, 90.604);
  glEnd();
}

void footPathLoop(){
    int i;
    float x=0,y=0,xScale=1,yScale=1;
    for(i=0;i<8;i++){
      x-=4.5;
      y-=18;
      xScale+=0.05;
      yScale+=0.05;
      glPushMatrix();
        glScalef(xScale,yScale,0);
        glTranslatef(x,y,0);
        footPath();
      glPopMatrix();
    }
}

void lGlassSide(){
  glColor3ub(200,183,183);
  glBegin(GL_POLYGON);
  glVertex2f(263.865, 543.613);
  glVertex2f(300.194, 576.141);
  glVertex2f(300.863, 509.941);
  glVertex2f(263.865, 490.877);
  glEnd();
}

void lGlassSideW(){
  glColor3ub(213,246,255);
  glBegin(GL_POLYGON);
  glVertex2f(265.865, 541.613);
  glVertex2f(298.194, 572.141);
  glVertex2f(298.863, 513.941);
  glVertex2f(265.865, 495.877);
  glEnd();
}

void lGlassMid(){
  glColor3ub(227,222,219);
  glBegin(GL_POLYGON);
  glVertex2f(300.194, 576.141);
  glVertex2f(534.097, 608.244);
  glVertex2f(534.097, 532.969);
  glVertex2f(518.623, 532.877);
  glVertex2f(300.194, 548.635);
  glEnd();
}

void lGlassMidOne(){
  glColor3ub(213,246,255);
  glBegin(GL_POLYGON);
  glVertex2f(305.870, 573.394);
  glVertex2f(354.508, 580.414);
  glVertex2f(354.508, 550.097);
  glVertex2f(305.870, 553.335);
  glEnd();
}

void lGlassMidTwo(){
  glColor3ub(213,246,255);
  glBegin(GL_POLYGON);
  glVertex2f(361.267, 581.470);
  glVertex2f(416.607, 589.553);
  glVertex2f(416.607, 545.239);
  glVertex2f(361.267, 549.464);
  glEnd();
}

void lGlassMidThree(){
  glColor3ub(213,246,255);
  glBegin(GL_POLYGON);
  glVertex2f(423.366, 590.186);
  glVertex2f(474.904, 596.945);
  glVertex2f(474.904, 540.592);
  glVertex2f(423.366, 544.817);
  glEnd();
}

void lGlassMidFour(){
  glColor3ub(213,246,255);
  glBegin(GL_POLYGON);
  glVertex2f(480.607, 598.001);
  glVertex2f(528.554, 604.127);
  glVertex2f(528.554, 535.946);
  glVertex2f(481.663, 540.804);
  glEnd();
}

void lRoofRight(){
  glColor3ub(249,249,249);
  glBegin(GL_POLYGON);
  glVertex2f(276.895, 579.128);
  glVertex2f(558.395, 617.725);
  glVertex2f(534.694, 606.452);
  glVertex2f(300.194, 576.141);
  glEnd();
}

void lRoofLeft(){
  glColor3ub(242,242,242);
  glBegin(GL_POLYGON);
  glVertex2f(276.995, 579.128);
  glVertex2f(300.194, 576.141);
  glVertex2f(263.865, 543.613);
  glVertex2f(238.518, 539.811);
  glEnd();
}

void lRoofUpLeft(){
  glColor3ub(26,26,26);
  glBegin(GL_POLYGON);
  glVertex2f(234.751, 545.755);
  glVertex2f(275.025, 585.767);
  glVertex2f(276.995, 579.128);
  glVertex2f(238.518, 539.811);
  glEnd();
}

void lRoofUpRight(){
  glColor3ub(51,51,51);
  glBegin(GL_POLYGON);
  glVertex2f(275.025, 585.767);
  glVertex2f(558.409, 623.065);
  glVertex2f(560.202, 617.390);
  glVertex2f(276.995, 579.128);
  glEnd();
}

void lWallMid(){
  glColor3ub(77,77,77);
  glBegin(GL_POLYGON);
  glVertex2f(300.194, 548.635);
  glVertex2f(516.623, 533.877);
  glVertex2f(516.623, 200.624);
  glVertex2f(307.265, 172.398);
  glEnd();
}

void lWallSideMid(){
  glColor3ub(26,26,26);
  glBegin(GL_POLYGON);
  glVertex2f(230.577, 473.548);
  glVertex2f(301.863, 511.941);
  glVertex2f(307.265, 172.398);
  glVertex2f(234.870, 255.103);
  glEnd();
}

void lWallSideMidGlassOne(){
  glColor3ub(213,246,255);
  glBegin(GL_POLYGON);
  glVertex2f(239.459, 309.853);
  glVertex2f(263.655, 294.320);
  glVertex2f(264.079, 226.008);
  glVertex2f(240.093, 253.583);
  glEnd();
}

void lWallSideMidGlassTwo(){
  glColor3ub(213,246,255);
  glBegin(GL_POLYGON);
  glVertex2f(268.302, 291.960);
  glVertex2f(281.820, 282.568);
  glVertex2f(282.209, 207.577);
  glVertex2f(268.289, 222.048);
  glEnd();
}

void lWallSideMidGlassUp(){
  glColor3ub(213,246,255);
  glBegin(GL_POLYGON);
  glVertex2f(238.340, 418.872);
  glVertex2f(279.757, 423.652);
  glVertex2f(281.445, 326.571);
  glVertex2f(239.028, 345.091);
  glEnd();
}

void lWallSidefront(){
  glColor3ub(26,26,26);
  glBegin(GL_POLYGON);
  glVertex2f(397.780, 443.365);
  glVertex2f(457.066, 452.222);
  glVertex2f(461.680, 142.309);
  glVertex2f(399.713, 184.427);
  glEnd();
}

//Work here later
void lWallSidefrontGlass(){
  glColor3ub(213,246,255);
  glBegin(GL_POLYGON);
  glVertex2f(404.049, 271.320);
  glVertex2f(415.032, 266.250);
  glVertex2f(415.877, 181.340);
  glVertex2f(404.049, 188.944);
  glEnd();
}

void lWallFront(){
  glColor3ub(51,51,51);
  glBegin(GL_POLYGON);
  glVertex2f(457.066, 452.222);
  glVertex2f(592.254, 448.816);
  glVertex2f(595.086, 183.418);
  glVertex2f(461.185, 163.270);
  glEnd();
}

//Work here later
void lGlassFront(){
  glColor3ub(213,246,255);
  glBegin(GL_POLYGON);
  glVertex2f(484.565, 401.717);
  glVertex2f(576.234, 403.189);
  glVertex2f(576.234, 306.239);
  glVertex2f(485.304, 300.008);
  glEnd();
}

void lFloor(){
  glColor3ub(249,249,249);
  glBegin(GL_POLYGON);
  glVertex2f(462.185, 163.270);
  glVertex2f(595.086, 183.418);
  glVertex2f(649.371, 158.418);
  glVertex2f(504.267, 133.078);
  glEnd();
}

void lFloorSt(){
  glColor3ub(249,249,249);
  glBegin(GL_POLYGON);
  glVertex2f(506.718, 125.846);
  glVertex2f(652.433, 153.878);
  glVertex2f(657.988, 149.079);
  glVertex2f(512.274, 121.048);
  glEnd();
}

void lFloorStd(){
  glColor3ub(26,26,26);
  glBegin(GL_POLYGON);
  glVertex2f(504.267, 133.078);
  glVertex2f(649.371, 160.418);
  glVertex2f(650.686, 153.878);
  glVertex2f(506.718, 125.846);
  glEnd();
}

void lFloorStb(){
  glColor3ub(26,26,26);
  glBegin(GL_POLYGON);
  glVertex2f(461.345, 163.598);
  glVertex2f(519.586, 125.789);
  glVertex2f(522.586, 103.499);
  glVertex2f(461.680, 142.309);
  glEnd();
}

void lDoor(){
  glBegin(GL_POLYGON);
  glVertex2f(493.183, 256.906);
  glVertex2f(533.738, 260.336);
  glVertex2f(533.160, 175.848);
  glVertex2f(493.141, 170.357);
  glEnd();
}

void lRoofBottomLeft(){
  glColor3ub(20,20,20);
  glBegin(GL_POLYGON);
  glVertex2f(459.247, 278.565);
  glVertex2f(495.391, 269.356);
  glVertex2f(495.391, 255.615);
  glVertex2f(459.247, 264.875);
  glEnd();
}

void lRoofBottomRight(){
  glColor3ub(20,20,20);
  glBegin(GL_POLYGON);
  glVertex2f(495.391, 269.356);
  glVertex2f(643.551, 281.953);
  glVertex2f(643.551, 268.759);
  glVertex2f(495.391, 255.615);
  glEnd();
}

void lRoofBottomUp(){
  glColor3ub(77,77,77);
  glBegin(GL_POLYGON);
  glVertex2f(459.247, 278.565);
  glVertex2f(595.260, 288.772);
  glVertex2f(643.551, 281.953);
  glVertex2f(495.391, 269.356);
  glEnd();
}

void mWallSideLeft(){
  glColor3ub(26,26,26);
  glBegin(GL_POLYGON);
  glVertex2f(517.220, 575.593);
  glVertex2f(595.344, 607.480);
  glVertex2f(595.766, 477.579);
  glVertex2f(620.268, 479.480);
  glVertex2f(620.683, 328.474);
  glVertex2f(667.333, 320.955);
  glVertex2f(667.333, 131.280);
  glVertex2f(516.623, 200.624);
  glEnd();
}

void mGlassSide(){
  glColor3ub(213,246,255);
  glBegin(GL_POLYGON);
  glVertex2f(521.551, 550.593);
  glVertex2f(537.681, 555.671);
  glVertex2f(538.279, 508.177);
  glVertex2f(521.850, 505.189);
  glEnd();
}

void mWallTh(){
  glColor3ub(51,51,51);
  glBegin(GL_POLYGON);
  glVertex2f(595.344, 607.480);
  glVertex2f(782.653, 581.088);
  glVertex2f(782.653, 471.333);
  glVertex2f(595.766, 477.579);
  glEnd();
}

void mGlassTh(){
  glColor3ub(213,246,255);
  glBegin(GL_POLYGON);
  glVertex2f(632.835, 569.366);
  glVertex2f(662.617, 565.987);
  glVertex2f(662.617, 475.796);
  glVertex2f(632.835, 479.176);
  glEnd();
}

void mGlassThFrame(){
  glColor3ub(227,222,219);
  glBegin(GL_POLYGON);
  glVertex2f(632.835, 569.366);
  glVertex2f(632.835, 479.176);
  glVertex2f(629.102, 479.651);
  glVertex2f(629.102, 569.789);
  glEnd();
}

void mBalconyThLeft(){
  glColor3ub(227,222,219);
  glBegin(GL_POLYGON);
  glVertex2f(623.789, 520.069);
  glVertex2f(623.789, 478.399);
  glVertex2f(620.055, 478.548);
  glVertex2f(620.055, 515.439);
  glVertex2f(598.847, 511.406);
  glVertex2f(598.847, 515.588);
  glEnd();
}

void mBalconyGlassThLeft(){
  glColor4ub(213,246,255,200);
  glBegin(GL_POLYGON);
  glVertex2f(598.847, 511.406);
  glVertex2f(620.055, 515.439);
  glVertex2f(620.055, 478.743);
  glVertex2f(598.847, 477.189);
  glEnd();
}

void mBalconyThRightL(){
  glColor3ub(200,183,183);
  glBegin(GL_POLYGON);
  glVertex2f(623.789, 520.517);
  glVertex2f(628.436, 515.553);
  glVertex2f(628.436, 478.506);
  glVertex2f(623.789, 478.399);
  glEnd();
}

void mBalconyThRightM(){
  glColor3ub(200,183,183);
  glBegin(GL_POLYGON);
  glVertex2f(623.789, 520.517);
  glVertex2f(741.783, 510.627);
  glVertex2f(738.298, 508.208);
  glVertex2f(628.436, 515.553);
  glEnd();
}

void mBalconyThRightR(){
  glColor3ub(200,183,183);
  glBegin(GL_POLYGON);
  glVertex2f(741.783, 510.627);
  glVertex2f(741.783, 473.885);
  glVertex2f(738.298, 473.674);
  glVertex2f(738.298, 508.208);
  glEnd();
}

void mBalconyGlassThRight(){
  glColor4ub(213,246,255,200);
  glBegin(GL_POLYGON);
  glVertex2f(628.436, 515.553);
  glVertex2f(738.298, 508.208);
  glVertex2f(738.298, 473.521);
  glVertex2f(628.436, 479.073);
  glEnd();
}

void mRoofLeft(){
  glColor3ub(255,255,255);
  glBegin(GL_POLYGON);
  glVertex2f(508.259, 577.684);
  glVertex2f(591.162, 612.856);
  glVertex2f(595.344, 607.480);
  glVertex2f(517.220, 575.593);
  glEnd();
}

void mRoofRight(){
  glColor3ub(255,255,255);
  glBegin(GL_POLYGON);
  glVertex2f(591.162, 612.856);
  glVertex2f(785.188, 585.101);
  glVertex2f(782.653, 581.088);
  glVertex2f(595.344, 607.480);
  glEnd();
}

void mRoofRightCorner(){
  glColor3ub(255,255,255);
  glBegin(GL_POLYGON);
  glVertex2f(785.188, 585.101);
  glVertex2f(802.513, 581.450);
  glVertex2f(783.925, 576.803);
  glVertex2f(782.653, 581.088);
  glEnd();
}

void mRoofUpLeft(){
  glColor3ub(51,51,51);
  glBegin(GL_POLYGON);
  glVertex2f(507.064, 583.359);
  glVertex2f(591.052, 619.442);
  glVertex2f(591.162, 612.856);
  glVertex2f(508.259, 577.684);
  glEnd();
}

void mRoofUpRight(){
  glColor3ub(26,26,26);
  glBegin(GL_POLYGON);
  glVertex2f(591.052, 619.442);
  glVertex2f(802.513, 588.619);
  glVertex2f(802.513, 581.450);
  glVertex2f(591.162, 612.856);
  glEnd();
}

void mWallSe(){
  glColor3ub(51,51,51);
  glBegin(GL_POLYGON);
  glVertex2f(620.268, 479.480);
  glVertex2f(745.376, 473.929);
  glVertex2f(745.376, 336.213);
  glVertex2f(620.683, 328.474);
  glEnd();
}

void mGlassSe(){
  glColor3ub(213,246,255);
  glBegin(GL_POLYGON);
  glVertex2f(660.083, 424.838);
  glVertex2f(690.710, 425.892);
  glVertex2f(690.710, 337.546);
  glVertex2f(660.083, 335.264);
  glEnd();
}

void mGlassSeFrame(){
  glColor3ub(227,222,219);
  glBegin(GL_POLYGON);
  glVertex2f(657.225, 424.896);
  glVertex2f(660.083, 424.838);
  glVertex2f(660.083, 335.264);
  glVertex2f(657.225, 335.143);
  glEnd();
}

void mBalconySeLeftUp(){
  glColor3ub(227,222,219);
  glBegin(GL_POLYGON);
  glVertex2f(622.926, 370.708);
  glVertex2f(667.251, 367.117);
  glVertex2f(667.251, 363.632);
  glVertex2f(622.926, 367.223);
  glEnd();
}

void mBalconySeRightLeft(){
  glColor3ub(200,183,183);
  glBegin(GL_POLYGON);
  glVertex2f(667.251, 367.117);
  glVertex2f(671.423, 367.117);
  glVertex2f(671.423, 324.609);
  glVertex2f(667.251, 324.345);
  glEnd();
}

void mBalconySeRightUp(){
  glColor3ub(200,183,183);
  glBegin(GL_POLYGON);
  glVertex2f(671.423, 367.117);
  glVertex2f(793.986, 370.813);
  glVertex2f(793.986, 366.962);
  glVertex2f(671.423, 363.266);
  glEnd();
}

void mBalconySeRightMid(){
  glColor3ub(200,183,183);
  glBegin(GL_POLYGON);
  glVertex2f(728.509, 367.582);
  glVertex2f(732.835, 367.660);
  glVertex2f(732.835, 328.156);
  glVertex2f(728.509, 328.156);
  glEnd();
}

void mBalconySeRightRight(){
  glColor3ub(200,183,183);
  glBegin(GL_POLYGON);
  glVertex2f(785.831, 368.254);
  glVertex2f(790.157, 368.332);
  glVertex2f(790.157, 331.309);
  glVertex2f(785.831, 331.231);
  glEnd();
}

void mBalconySeRightMidInside(){
  glColor3ub(227,222,219);
  glBegin(GL_POLYGON);
  glVertex2f(725.626, 365.699);
  glVertex2f(729.135, 365.699);
  glVertex2f(729.509, 328.156);
  glVertex2f(725.626, 328.899);
  glEnd();
}

void mBalconySeRightRightInside(){
  glColor3ub(227,222,219);
  glBegin(GL_POLYGON);
  glVertex2f(782.694, 368.271);
  glVertex2f(785.831, 368.254);
  glVertex2f(785.831, 331.231);
  glVertex2f(782.694, 331.754);
  glEnd();
}

void mBalconySeLeft(){
  glColor3ub(227,222,219);
  glBegin(GL_POLYGON);
  glVertex2f(663.741, 363.819);
  glVertex2f(667.251, 363.632);
  glVertex2f(667.251, 324.345);
  glVertex2f(663.741, 324.532);
  glEnd();
}

void mBalconySeLeftGlass(){
  glColor4ub(213,246,255,200);
  glBegin(GL_POLYGON);
  glVertex2f(622.926, 367.223);
  glVertex2f(663.741, 364.819);
  glVertex2f(663.741, 324.532);
  glVertex2f(622.926, 331.949);
  glEnd();
}

void mBalconySeMidOneGlass(){
  glColor4ub(213,246,255,200);
  glBegin(GL_POLYGON);
  glVertex2f(671.423, 364.266);
  glVertex2f(728.135, 365.699);
  glVertex2f(728.509, 328.156);
  glVertex2f(671.423, 324.609);
  glEnd();
}

void mBalconySeMidTwoGlass(){
  glColor4ub(213,246,255,200);
  glBegin(GL_POLYGON);
  glVertex2f(732.835, 365.865);
  glVertex2f(785.831, 368.254);
  glVertex2f(785.831, 331.231);
  glVertex2f(732.835, 328.156);
  glEnd();
}

void mFloorSe(){
  glColor3ub(77,77,77);
  glBegin(GL_POLYGON);
  glVertex2f(618.281, 329.312);
  glVertex2f(745.376, 336.213);
  glVertex2f(796.404, 330.398);
  glVertex2f(667.333, 320.955);
  glEnd();
}

void mWallFi(){
  glColor3ub(51,51,51);
  glBegin(GL_POLYGON);
  glVertex2f(667.333, 320.955);
  glVertex2f(794.910, 331.249);
  glVertex2f(794.910, 156.619);
  glVertex2f(667.333, 131.280);
  glEnd();
}

void mGlassFi(){
  glColor3ub(213,246,255);
  glBegin(GL_POLYGON);
  glVertex2f(677.333, 270.955);
  glVertex2f(784.910, 281.249);
  glVertex2f(784.910, 180.619);
  glVertex2f(677.333, 161.280);
  glEnd();
}

void rWallSideUp(){
  glColor3ub(26,26,26);
  glBegin(GL_POLYGON);
  glVertex2f(718.151, 536.357);
  glVertex2f(797.143, 552.533);
  glVertex2f(797.406, 330.398);
  glVertex2f(718.151, 337.955);
  glEnd();
}

void rWallSideBottom(){
  glColor3ub(26,26,26);
  glBegin(GL_POLYGON);
  glVertex2f(795.404, 331.398);
  glVertex2f(856.241, 323.632);
  glVertex2f(856.241, 138.026);
  glVertex2f(794.910, 156.619);
  glEnd();
}

void rWallFrontBottomLeft(){
  glColor3ub(51,51,51);
  glBegin(GL_POLYGON);
  glVertex2f(856.241, 322.632);
  glVertex2f(1051.434, 334.352);
  glVertex2f(1050.732, 289.883);
  glVertex2f(924.676, 275.545);
  glVertex2f(924.676, 152.389);
  glVertex2f(856.241, 138.026);
  glEnd();
}

void rWallFrontBottomRight(){
  glColor3ub(51,51,51);
  glBegin(GL_POLYGON);
  glVertex2f(1050.434, 333.852);
  glVertex2f(1065.921, 334.696);
  glVertex2f(1063.753, 185.349);
  glVertex2f(1049.537, 181.470);
  glEnd();
}

void rWallFrontUp(){
  glColor3ub(51,51,51);
  glBegin(GL_POLYGON);
  glVertex2f(797.150, 478.901);
  glVertex2f(991.760, 466.953);
  glVertex2f(990.070, 343.178);
  glVertex2f(796.403, 330.398);
  glEnd();
}

void rBalconyFrameLeftUp(){
  glColor3ub(227,222,219);
  glBegin(GL_POLYGON);
  glVertex2f(801.689, 371.175);
  glVertex2f(858.266, 368.301);
  glVertex2f(858.266, 364.567);
  glVertex2f(801.689, 367.142);
  glEnd();
}

void rBalconyFrameLeftRight(){
  glColor3ub(227,222,219);
  glBegin(GL_POLYGON);
  glVertex2f(854.308, 368.301);
  glVertex2f(858.266, 368.301);
  glVertex2f(858.293, 326.557);
  glVertex2f(854.308, 328.010);
  glEnd();
}

void rBalconyFrameRightLeft(){
  glColor3ub(200,183,183);
  glBegin(GL_POLYGON);
  glVertex2f(858.266, 368.301);
  glVertex2f(862.174, 368.406);
  glVertex2f(862.174, 327.856);
  glVertex2f(858.293, 326.557);
  glEnd();
}

void rBalconyFrameRightUp(){
  glColor3ub(200,183,183);
  glBegin(GL_POLYGON);
  glVertex2f(862.174, 368.406);
  glVertex2f(1060.875, 375.178);
  glVertex2f(1060.875, 371.444);
  glVertex2f(862.174, 364.672);
  glEnd();
}

void rBalconyFrameRightRight(){
  glColor3ub(200,183,183);
  glBegin(GL_POLYGON);
  glVertex2f(1055.556, 371.295);
  glVertex2f(1060.875, 371.444);
  glVertex2f(1060.875, 335.444);
  glVertex2f(1055.556, 335.593);
  glEnd();
}

void rBalconyFrameRightInside(){
  glColor3ub(227,222,219);
  glBegin(GL_POLYGON);
  glVertex2f(975.292, 377.352);
  glVertex2f(1060.875, 375.178);
  glVertex2f(1060.875, 371.444);
  glVertex2f(975.292, 373.618);
  glEnd();
}

void rBalconyFrameRightRightInside(){
  glColor3ub(227,222,219);
  glBegin(GL_POLYGON);
  glVertex2f(1051.224, 375.880);
  glVertex2f(1055.556, 375.029);
  glVertex2f(1055.556, 336.444);
  glVertex2f(1051.224, 337.294);
  glEnd();
}

void rBalconyGlassLeft(){
  glColor4ub(213,246,255,200);
  glBegin(GL_POLYGON);
  glVertex2f(801.689, 371.175);
  glVertex2f(858.266, 368.301);
  glVertex2f(858.293, 326.557);
  glVertex2f(801.689, 331.956);
  glEnd();
}

void rBalconyGlassMid(){
  glColor4ub(213,246,255,200);
  glBegin(GL_POLYGON);
  glVertex2f(862.174, 364.672);
  glVertex2f(1055.556, 371.295);
  glVertex2f(1055.556, 336.444);
  glVertex2f(862.174, 327.856);
  glEnd();
}

void rBalconyGlassRight(){
  glColor4ub(213,246,255,200);
  glBegin(GL_POLYGON);
  glVertex2f(975.292, 374.118);
  glVertex2f(1055.556, 371.295);
  glVertex2f(1055.556, 340.154);
  glVertex2f(975.292, 345.092);
  glEnd();
}

void rGlassFrameLeftTh(){
  glColor3ub(200,183,183);
  glBegin(GL_POLYGON);
  glVertex2f(797.143, 552.533);
  glVertex2f(806.238, 550.890);
  glVertex2f(806.238, 478.200);
  glVertex2f(797.151, 478.901);
  glEnd();
}

void rGlassFrameUpTh(){
  glColor3ub(200,183,183);
  glBegin(GL_POLYGON);
  glVertex2f(806.238, 550.890);
  glVertex2f(992.773, 493.348);
  glVertex2f(992.773, 489.757);
  glVertex2f(806.238, 547.300);
  glEnd();
}

void rGlassFrameRightTh(){
  glColor3ub(200,183,183);
  glBegin(GL_POLYGON);
  glVertex2f(989.636, 491.251);
  glVertex2f(992.773, 489.757);
  glVertex2f(992.773, 467.653);
  glVertex2f(989.636, 466.951);
  glEnd();
}

void rGlassFrameBottomTh(){
  glColor3ub(200,183,183);
  glBegin(GL_POLYGON);
  glVertex2f(806.238, 483.424);
  glVertex2f(989.636, 471.648);
  glVertex2f(990.636, 466.451);
  glVertex2f(806.238, 478.200);
  glEnd();
}

void rGlassFrameMidOneTh(){
  glColor3ub(200,183,183);
  glBegin(GL_POLYGON);
  glVertex2f(857.379, 532.257);
  glVertex2f(860.155, 530.273);
  glVertex2f(860.155, 478.895);
  glVertex2f(857.379, 479.246);
  glEnd();
}

void rGlassFrameMidTwoTh(){
  glColor3ub(200,183,183);
  glBegin(GL_POLYGON);
  glVertex2f(905.666, 517.426);
  glVertex2f(909.442, 515.441);
  glVertex2f(908.471, 475.983);
  glVertex2f(905.695, 476.333);
  glEnd();
}

void rGlassFrameMidThreeTh(){
  glColor3ub(200,183,183);
  glBegin(GL_POLYGON);
  glVertex2f(947.635, 505.627);
  glVertex2f(951.411, 502.642);
  glVertex2f(950.590, 472.846);
  glVertex2f(947.813, 473.197);
  glEnd();
}

void rGlassFrameUpInsideTh(){
  glColor3ub(227,222,219);
  glBegin(GL_POLYGON);
  glVertex2f(806.238, 547.300);
  glVertex2f(989.636, 491.251);
  glVertex2f(989.636, 488.264);
  glVertex2f(806.238, 544.342);
  glEnd();
}

void rGlassFrameMidInsideOneTh(){
  glColor3ub(227,222,219);
  glBegin(GL_POLYGON);
  glVertex2f(853.154, 531.152);
  glVertex2f(857.379, 531.257);
  glVertex2f(857.379, 479.246);
  glVertex2f(853.154, 480.353);
  glEnd();
}

void rGlassFrameMidInsideTwoTh(){
  glColor3ub(227,222,219);
  glBegin(GL_POLYGON);
  glVertex2f(903.026, 515.314);
  glVertex2f(905.666, 517.426);
  glVertex2f(905.695, 476.333);
  glVertex2f(903.026, 476.872);
  glEnd();
}

void rGlassFrameMidInsideThreeTh(){
  glColor3ub(227,222,219);
  glBegin(GL_POLYGON);
  glVertex2f(944.678, 502.247);
  glVertex2f(947.635, 504.627);
  glVertex2f(947.813, 473.197);
  glVertex2f(944.678, 473.838);
  glEnd();
}

void rGlassOneTh(){
  glColor4ub(213,246,255,200);
  glBegin(GL_POLYGON);
  glVertex2f(806.238, 547.300);
  glVertex2f(857.379, 531.257);
  glVertex2f(857.379, 479.246);
  glVertex2f(806.238, 483.424);
  glEnd();
}

void rGlassTwoTh(){
  glColor4ub(213,246,255,200);
  glBegin(GL_POLYGON);
  glVertex2f(860.155, 530.273);
  glVertex2f(905.666, 516.426);
  glVertex2f(905.695, 476.333);
  glVertex2f(860.155, 478.895);
  glEnd();
}

void rGlassThreeTh(){
  glColor4ub(213,246,255,200);
  glBegin(GL_POLYGON);
  glVertex2f(909.442, 515.441);
  glVertex2f(947.635, 503.627);
  glVertex2f(947.813, 473.197);
  glVertex2f(908.471, 475.983);
  glEnd();
}

void rGlassFourTh(){
  glColor4ub(213,246,255,200);
  glBegin(GL_POLYGON);
  glVertex2f(951.411, 501.642);
  glVertex2f(989.636, 491.251);
  glVertex2f(989.636, 471.648);
  glVertex2f(950.590, 472.846);
  glEnd();
}

void rWallSideFloorSe(){
  glColor3ub(77,77,77);
  glBegin(GL_POLYGON);
  glVertex2f(796.404, 330.398);
  glVertex2f(990.070, 343.178);
  glVertex2f(1065.921, 334.696);
  glVertex2f(856.241, 322.632);
  glEnd();
}

void rWallSideBottomRi(){
  glColor3ub(26,26,26);
  glBegin(GL_POLYGON);
  glVertex2f(996.068, 303.376);
  glVertex2f(1050.732, 289.883);
  glVertex2f(1049.537, 181.470);
  glVertex2f(996.068, 197.029);
  glEnd();
}

void rWallSideBack(){
  glColor3ub(249,249,249);
  glBegin(GL_POLYGON);
  glVertex2f(797.151, 484.901);
  glVertex2f(991.760, 488.759);
  glVertex2f(991.760, 466.953);
  glVertex2f(797.151, 478.901);
  glEnd();
}

void door(){
  glColor3ub(249,249,249);
  lDoor();

      glColor3ub(249,249,249);
      glPushMatrix();
        glTranslatef(41.5,6,0);
        lDoor();
      glPopMatrix();

      /*
       * Handle 1 */
      glColor3ub(26,26,26);
      glPushMatrix();
        glTranslatef(420,170,0);
        glScalef(.2,.2,0);
        lDoor();
      glPopMatrix();

      /*
       * Handle 2 */
      glColor3ub(26,26,26);
      glPushMatrix();
        glTranslatef(444,173,0);
        glScalef(.2,.2,0);
        lDoor();
      glPopMatrix();
}

void rDoor(){
  glBegin(GL_POLYGON);
  glVertex2f(900.023, 421.072);
  glVertex2f(971.521, 420.121);
  glVertex2f(971.521, 343.442);
  glVertex2f(900.023, 338.267);
  glEnd();
}

void rDoorUp(){
  /*
  * Doors */
  glPushMatrix();
  glColor3ub(233,233,233);
    glTranslatef(-30,0,0);
    rDoor();
  glPopMatrix();

  /*
  * Separator */
  glPushMatrix();
    glColor3ub(26,26,26);
    glTranslatef(878,1,0);
    glScalef(.03,1,0);
    rDoor();
  glPopMatrix();

  /*
  * Separator L */
  glPushMatrix();
    glColor3ub(26,26,26);
    glTranslatef(842,2,0);
    glScalef(.03,1,0);
    rDoor();
  glPopMatrix();

  /*
  * Separator R */
  glPushMatrix();
    glColor3ub(26,26,26);
    glTranslatef(915,0,0);
    glScalef(.03,1,0);
    rDoor();
  glPopMatrix();
}

void house(){

  lGlassSide();

  lGlassSideW();

  lRoofUpLeft();

  lRoofUpRight();

  lRoofRight();

  lRoofLeft();

  lGlassMid();

  lGlassMidOne();

  lGlassMidTwo();

  lGlassMidThree();

  lGlassMidFour();

  lWallMid();

  lWallSideMid();

  lWallSideMidGlassUp();

  lWallSideMidGlassOne();

  lWallSideMidGlassTwo();

  mWallSideLeft();

  mGlassSide();

  /*
   * 2 */
  glPushMatrix();
    glTranslatef(20,5,0);
    mGlassSide();
  glPopMatrix();
  lWallSidefront();

  /*
   * 3 */
  glPushMatrix();
    glTranslatef(42,11,0);
    mGlassSide();
  glPopMatrix();
  lWallSidefront();

  /*
   * 1st */
  lWallSidefrontGlass();

  /*
   * 2nd */
  glPushMatrix();
    glTranslatef(20,-12,0);
    lWallSidefrontGlass();
  glPopMatrix();

  /*
   * 3rd */
  glPushMatrix();
    glTranslatef(40,-24,0);
    lWallSidefrontGlass();
  glPopMatrix();

  lWallFront();

  lGlassFront();

  lFloor();

  mWallTh();

  mGlassTh();

  /*
   * right side */
  glPushMatrix();
    glTranslatef(30,-3.7,0);
    mGlassTh();
  glPopMatrix();

  mGlassThFrame();

  /*
   * mid */
  glPushMatrix();
    glTranslatef(345,-3.7,0);
    glScalef(.5,1,0);
    mGlassThFrame();
  glPopMatrix();

  /*
   * right */
  glPushMatrix();
    glTranslatef(30.675*2,-3.7*2,0);
    mGlassThFrame();
  glPopMatrix();

  mRoofLeft();

  mRoofRight();

  mRoofRightCorner();

  mRoofUpLeft();

  mRoofUpRight();

  rWallSideBack();

  rWallSideUp();

  mBalconyGlassThLeft();

  mBalconyThLeft();

  mBalconyGlassThRight();

  mBalconyThRightL();

  mBalconyThRightM();

  mBalconyThRightR();

  rWallSideBottom();

  mWallSe();

  mGlassSe();

  /*
   * right glass */
  glPushMatrix();
    glTranslatef(32,.7,0);
    mGlassSe();
  glPopMatrix();

  mGlassSeFrame();

  /*
   * middle */
  glPushMatrix();
    glTranslatef(230,1,0);
    glScalef(.7,1,0);
    mGlassSeFrame();
  glPopMatrix();

  /*
   * right */
  glPushMatrix();
    glTranslatef(65,1,0);
    mGlassSeFrame();
  glPopMatrix();

  mWallFi();

  mGlassFi();

  mFloorSe();

  mBalconySeLeft();

  mBalconySeLeftGlass();

  mBalconySeLeftUp();

  mBalconySeRightMidInside();

  mBalconySeMidOneGlass();

  mBalconySeRightRightInside();

  mBalconySeMidTwoGlass();

  mBalconySeRightLeft();

  mBalconySeRightUp();

  mBalconySeRightMid();

  mBalconySeRightRight();

  lFloorStb();

  lFloorStd();

  /*
   * second */
  glPushMatrix();
    glTranslatef(8,-11,0);
    lFloorStd();
  glPopMatrix();

  /*
   * first */
  glPushMatrix();
    glTranslatef(16,-21,0);
    lFloorStd();
  glPopMatrix();

  lFloorSt();

  /*
   * first stair */
  glPushMatrix();
    glTranslatef(7,-10,0);
    lFloorSt();
  glPopMatrix();

  /*
   * Main entrance */
  door();

  lRoofBottomRight();

  lRoofBottomLeft();

  lRoofBottomUp();

  rWallFrontUp();

  rDoorUp();

  rGlassFrameUpInsideTh();

  rGlassFrameMidInsideOneTh();

  rGlassFrameMidInsideTwoTh();

  rGlassFrameMidInsideThreeTh();

  rGlassOneTh();

  rGlassTwoTh();

  rGlassThreeTh();

  rGlassFourTh();

  rGlassFrameLeftTh();

  rGlassFrameUpTh();

  rGlassFrameRightTh();

  rGlassFrameBottomTh();

  rGlassFrameMidOneTh();

  rGlassFrameMidTwoTh();

  rGlassFrameMidThreeTh();

  rWallSideFloorSe();

  rBalconyGlassLeft();

  rBalconyFrameLeftUp();

  rBalconyGlassRight();

  rBalconyFrameRightRightInside();

  rBalconyGlassMid();

  rBalconyFrameLeftRight();

  rBalconyFrameRightLeft();

  rBalconyFrameRightInside();

  rBalconyFrameRightUp();

  rBalconyFrameRightRight();

  rWallSideBottomRi();

  rWallFrontBottomLeft();

  rWallFrontBottomRight();

}

void display()
{
  glEnable(GL_BLEND);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
  glEnable(GL_LINE_SMOOTH);
  glEnable(GL_POLYGON_SMOOTH);
  glEnable(GL_POINT_SMOOTH);
  glHint(GL_POLYGON_SMOOTH_HINT, GL_NICEST);

  sky();

  land();

  path();

  footPath();

  footPathLoop();

  house();

  glDisable( GL_BLEND );
  //glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
}

int main(void)
{
    GLFWwindow* window;
    if (!glfwInit()) exit(EXIT_FAILURE);

    window = glfwCreateWindow(WIDTH, HEIGHT, "Imah nu aing, aing nu saha?", NULL, NULL);

    if (!window)
    {
        glfwTerminate();
        exit(EXIT_FAILURE);
    }

    glfwMakeContextCurrent(window);
    glfwSwapInterval(1);
    glfwSetKeyCallback(window, key_callback);


    while (!glfwWindowShouldClose(window))
    {
        setup_viewport(window);

        display();

        glfwSwapBuffers(window);
        glfwPollEvents();
    }
    glfwDestroyWindow(window);
    glfwTerminate();

    exit(EXIT_SUCCESS);
}
